---
layout: post
title:  "A Practical Guide to Monetize Models for Apps"
date:   2018-07-03 2:52:21 -0500
author: "Cîrjan Raluca"
profile_link: "https://www.linkedin.com/in/raluca-cirjan/"
categories: Startup
lang: en
image:
  feature: monetize.png
  credit: "Freepik.com"
---

Creating a monetization strategy that is best suited for your app will bring you one step closer to accomplishing your business objectives. Before exploring any **monetization models**, keep in mind that you should always [start with ‘Why?’](https://viperdev.io/startup/2018/04/25/the-right-way-to-define-features-for-an-mvp).  

Why would a user buy a subscription or pay to download your app? This gives you the chance to realize what differentiates your app from others, what people need, and what features to focus on in order to meet their needs. **The monetization strategy should always be approached with the end user in mind.**  

Here are six app monetization strategies worth considering:  

1. **In-app advertising**  
This is one of the most popular app monetization strategies. Since people spend more time using their smartphones than any other device, companies are interested in allocating their advertising budgets to mobile apps. Banners, auto-play videos or sponsored posts can be displayed within a mobile app. Making sure that an ad is relevant to the audience while presenting it in an non-invasive manner is crucial, as this can result in sacrificing the overall user experience.  
One of the advantages of this technique for generating revenue through your app is that your app will remain free to the user, while you collect demographic and behavioral data which is of interest to ad networks. In order to achieve higher ad performance, you can combine different ad networks.
2. **Paid Apps**  
Using this monetization model, apps can only be downloaded if the user buys it from the app store. If you are planning to generate revenue through your app is by selling it, then make sure to emphasize what is unique about your product and superior to similar apps that are available for free. Using this model, you get 70% of profit, and the app stores take 30%.
This strategy can be challenging, as the users often see it as a barrier to download your app, especially since they have so many free options available. On the other hand, if you have a strong marketing strategy and your product offers added value over free, similar options, a paid app model is worth considering.
3. **Sponsorships**  
This model requires your audience to be large enough to attract brands to pay for exposure. The way sponsorships work is that advertisers provide rewards to your users if they complete certain in-app actions. This model works especially well with apps that are focused on certain industries, such as event apps.  
While the downside is that revenue is limited to value of partnership agreement and you usually do not earn more if the campaign performs better than expected, the good part is that this model is usually well-received by users as long as the content is relevant and valuable. It is best to consider sponsorships after having built a strong audience which will demonstrate your value to a sponsor.
4. **In-App Purchases**  
This strategy involves selling physical or virtual goods through your app to make profit. It is best suited for the gaming industry and one of its advantages is that you can mix this monetization model with others. The app remains free while users can choose whether or not to buy the products.  
You have the possibility of selling both virtual goods, such as lives for a character in a game, as well as physical products like T-shirts, stickers or any type of branded merchandise. Most app marketplaces charge 30% of their value, but the profit is still high especially when it comes to virtual goods.
5. **Freemium**  
A hybrid model between free and premium apps, Freemium is a well-known strategy where the app is available for free, but some of the features are locked and the user can unlock them by paying a certain amount of money. It can be anything from unlocking an ad-free version, to unlocking tools, new levels, filters, or memory space.  
If the premium features meet the user’s needs, people usually pay to experience the app’s full functionality. Many developers go with this particular strategy because they can prove their product’s value to the users without asking for money first. However, balance is key, as giving away too much will stop users from purchasing any extras, while insufficient free features can cause frustration.
6. **Subscriptions**  
Although similar in some ways to Freemium, when it comes to subscriptions, users get to try before they buy. They have the chance to watch a certain amount of content, but if they want to continue having access to it, they will be charged monthly for the content provided within an app.  
However, this model is not suitable for any type of product. The app must provide the type of content that encourages frequent and repeated use. It works well for entertainment, news and lifestyle (e.g. Netflix, The New York Times, Babbel).  


Understanding and even combining these models for monetizing an app, you can develop your own strategy to bring in the greatest possible return on investment from your app. For instance, a paid app can offer in-app purchases to enhance monetization. You can always choose from the following monetization models:  

* In-app advertising
* Paid / Premium Apps
* Sponsorships
* In-App Purchases
* Freemium
* Subscriptions  

Are you ready to [turn your business idea into an app?](https://viperdev.io/startup/2018/05/03/turn-your-business-idea-into-an-app) If you need help launching your app, we would be happy to advise you on your app’s technical requirements and strategy. [Book a call](https://calendly.com/viper) with us right now.
