---
layout: post
title:  "ViperDev holt 3. Platz beim Startup Slam von 12min.me"
date:   2018-08-29 2:00:00 -0500
author: "James Hiller"
profile_link: "https://www.linkedin.com/in/james-hiller-324734160"
categories: Startup
lang: de
image:
  feature: StartupSlamPic.png
  credit: "12min.me"
---

Es war wie immer eine gelungene Veranstaltung: Bereits einige Tage vor dem Event war die Veranstaltung auf meetup ausgebucht. 350 Anmeldungen gab es über die Plattform für den Startup Slam #8; die Zuschauer boten eine atemberaubende Kulisse. 12min.me, das ist ein regelmäßig stattfindendes Format in verschiedenen deutschen Großstädten, das Vortragenden die Möglichkeit eröffnet, innerhalb von 12 Minuten ihre Ideen oder in diesem Fall ihr Geschäftsmodell zu präsentieren. Die Zeit wird dabei gestoppt.

![StartUpSlamLasse](/assets/img/blog/StartupSlamLasse.jpg){:.img-feature}
*Gruppenbild mit allen Teilnehmern zum Abschluss des Startup Slams*      
Created by: 12min.me

Jedem interessierten Startup sei geraten, einmal an einem solchen Event teilzunehmen und sich der Jury zu stellen. Selbst wenn es nicht zu einem vorderen Platz reicht, ist das Feedback in den meisten Fällen äußerst hilfreich.
