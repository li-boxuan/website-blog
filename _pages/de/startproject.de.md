---
layout: page
permalink: /startproject/
lang: de
---

# Projekt Starten

Starte dein Projekt indem du entweder direkt [einen kostelosen Termin buchst](https://calendly.com/viper/kickstart/)
oder dieses Formular ausfüllst:

<form name="startproject" method="POST" netlify>
  <div class="input-field">
    <label for="name">Name</label><input type="text" name="name" id="name"/>
  </div>
  <div class="input-field">
    <label for="email">Email</label><input type="text" name="email" id="email"/>
  </div>
  <div class="input-field">
    <label for="phone">Telefon (Optional)</label><input type="text" name="phone" id="phone"/>
  </div>
  <div class="input-field">
    <label for="project">Projektbeschreibung (Optional)</label>
    <textarea name="project" id="project" class="materialize-textarea"></textarea>
  </div>
  <button type="submit" class="btn-large">Abschicken</button>
</form>

# Produktüberblick

{% include products_de.html %}
