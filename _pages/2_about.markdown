---
layout: cta-page
title: About
permalink: /about/
lang: en
---

## What is ViperDev

ViperDev is a software development startup from Hamburg, Germany.
We are developers. But we are also experienced founders.
That’s why we always put our clients needs first – not chargeability or expenses.

Together we find out what our customers really want.
Being on the same page we implement their software ideas into actual products fast and affordably.

## Core values

{% for value in site.data.values_en %}
<div id="flexbox" style="display: flex; align-items: center">
  <div>
    <img src="/assets/img/values/icon_{{ value.icon }}@2x.png" alt="icon_design" height="100" style="margin-right: 0 .75rem"/>
  </div>
  <div style="padding: 0.75em">
    <h3>{{ value.heading }}</h3>
    <p>{{ value.text }}</p>
  </div>
</div>
{% endfor %}

## Team

The core of our team is based in Hamburg, Germany, where ViperDev was founded in 2017. Additionally, we have built an international network of brilliant developers and inspiring founders working for us.

<div style="display: flex; flex-basis: content; flex-flow: row wrap; justify-content: center;">
  {% for member in site.data.team %}
      <a href="{{ member.linkedin }}" target="_blank">
        <div class="author" style="width: 300px">
          <img class="author-img" src="../assets/img/team/{{ member.name | downcase }}.jpg" alt="{{ member.name }}">
          <div class="author-details">
            <div class="name">{{ member.name }}</div>
            <div class="position">{{ member.position }}</div>
            <div class="position">{{ member.location }}</div>
            {% if member.mail %}
            <div class="position">
              <a href="mailto:{{ member.mail }}">Mail Me</a>
            </div>
            {% endif %}
          </div>
        </div>
      </a>
  {% endfor %}
</div>
